extends Control

onready var pipe_grid = $Center/PipeGrid

var game

func is_orb_lit(orb_type : int) -> bool:
	return pipe_grid.is_orb_lit(orb_type)

func set_active(active : bool) -> void:
	pipe_grid.set_active(active)

func pipe_changed() -> void:
	game.pipe_changed()

func _ready() -> void:
	pipe_grid.puzzle = self
