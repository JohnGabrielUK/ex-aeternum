extends GridContainer

const _Pipe : PackedScene = preload("res://object/Pipe.tscn")
const _PipeEmpty : PackedScene = preload("res://object/PipeEmpty.tscn")

var puzzle

func get_pipes() -> Array:
	var pipes : Array = []
	for child in get_children():
		if child is Pipe:
			pipes.append(child)
	return pipes

func is_orb_lit(orb_type : int) -> bool:
	for pipe in get_pipes():
		if pipe.orb_type == orb_type and pipe.orb_lit:
			return true
	return false

func set_active(active : bool) -> void:
	for pipe in get_pipes():
		pipe.active = active

func reset_pipes() -> void:
	for pipe in get_pipes():
		pipe.reset_burned()

func get_pipe_at_pos(pos : Vector2) -> Node:
	for pipe in get_pipes():
		if pipe.board_position == pos:
			return pipe
	return null

func try_to_burn_tile(target : Vector2, from_direction : Vector2) -> void:
	var pipe_target : Node = get_pipe_at_pos(target)
	if pipe_target != null:
		if not pipe_target.burned:
			pipe_target.get_burned(from_direction)

func refresh_connections() -> void:
	reset_pipes()
	for pipe in get_pipes():
		# Does this have an orb?
		if pipe.is_emitting():
			for direction in pipe.get_spread_direction():
				var target_pos : Vector2 = pipe.board_position + direction
				try_to_burn_tile(target_pos, direction * -1)
	# Maybe some doors need to change
	puzzle.pipe_changed()

func fill_grid(pipe_data : Array, orb_data : Array, grid_width : int) -> void:
	columns = grid_width
	for i in range(0, len(pipe_data)):
		var x : int = i % grid_width
		var y : int = i / grid_width
		var pipe_type : int = pipe_data[i]
		var orb_type : int = orb_data[i]
		if pipe_type != -1:
			var pipe : Control = _Pipe.instance()
			pipe.pipe_type = pipe_type
			pipe.board_position = Vector2(x, y)
			pipe.grid = self
			add_child(pipe)
			pipe.set_orb_type(orb_type)
		else:
			add_child(_PipeEmpty.instance())
