extends Control

class_name Pipe

const _Flame = preload("res://object/effects/Flame.tscn")

onready var sprite : Sprite = $Sprite
onready var sprite_orb : Sprite = $Sprite_Orb
onready var sprite_orb_shine : Sprite = $Sprite_Orb_Shine
onready var audio_spin : AudioStreamPlayer = $Audio_Spin

onready var flame_emitters : Array = [
	[$Sprite/EmitPoint_Center, $Sprite/EmitPoint_East1, $Sprite/EmitPoint_East2, $Sprite/EmitPoint_South1, $Sprite/EmitPoint_South2],
	[$Sprite/EmitPoint_Center, $Sprite/EmitPoint_West1, $Sprite/EmitPoint_West2, $Sprite/EmitPoint_East1, $Sprite/EmitPoint_East2],
	[$Sprite/EmitPoint_Center, $Sprite/EmitPoint_West1, $Sprite/EmitPoint_West2, $Sprite/EmitPoint_East1, $Sprite/EmitPoint_East2, $Sprite/EmitPoint_South1, $Sprite/EmitPoint_South2],
	[$Sprite/EmitPoint_Center, $Sprite/EmitPoint_West1, $Sprite/EmitPoint_West2, $Sprite/EmitPoint_East1, $Sprite/EmitPoint_East2, $Sprite/EmitPoint_North1, $Sprite/EmitPoint_South2, $Sprite/EmitPoint_North1, $Sprite/EmitPoint_South2],
	[$Sprite/EmitPoint_Center, $Sprite/EmitPoint_East1, $Sprite/EmitPoint_East2]
]

var sprite_target_rotation : float = 0
var rotation : int = 0

var shine_animation : float = 0.0

var board_position : Vector2
var pipe_type : int = 0
var orb_type : int = -1
var burned : bool = false
var orb_lit : bool = false

var active : bool = false

var grid : GridContainer

signal spread_fire

func set_pipe_rotation(rot : int) -> void:
	rotation = rot
	sprite_target_rotation = rotation * (PI/2)
	sprite.rotation = sprite_target_rotation

func set_orb_type(type) -> void:
	orb_type = type
	sprite_orb.visible = type != -1
	if type != -1:
		sprite_orb.frame = type

func is_emitting() -> bool:
	if orb_type == 3: return true
	return false

func get_spread_direction() -> Array:
	match pipe_type:
		# L block
		0:
			match rotation % 4:
				0: return [Vector2.DOWN, Vector2.RIGHT]
				1: return [Vector2.DOWN, Vector2.LEFT]
				2: return [Vector2.UP, Vector2.LEFT]
				3: return [Vector2.UP, Vector2.RIGHT]
		# line piece
		1:
			match rotation % 2:
				0: return [Vector2.LEFT, Vector2.RIGHT]
				1: return [Vector2.UP, Vector2.DOWN]
		# T block
		2:
			match rotation % 4:
				0: return [Vector2.DOWN, Vector2.LEFT, Vector2.RIGHT]
				1: return [Vector2.UP, Vector2.DOWN, Vector2.LEFT]
				2: return [Vector2.UP, Vector2.LEFT, Vector2.RIGHT]
				3: return [Vector2.UP, Vector2.DOWN, Vector2.RIGHT]
		# crossroads
		3:
			return [Vector2.UP, Vector2.DOWN, Vector2.LEFT, Vector2.RIGHT]
		# entry piece
		4:
			match rotation % 4:
				0: return [Vector2.RIGHT]
				1: return [Vector2.DOWN]
				2: return [Vector2.LEFT]
				3: return [Vector2.UP]
	return []

func get_burned(from_direction : Vector2) -> void:
	# If the 'fire' has already reached us, do nothing
	if burned: return
	burned = true
	# If we aren't connected to the tile from which the 'fire' is coming, do nothing
	var spread_directions : Array = get_spread_direction()
	if not from_direction in spread_directions:
		return
	# Spread the fire
	for direction in get_spread_direction():
		var target : Vector2 = board_position + direction
		grid.try_to_burn_tile(target, direction * -1)
	# Sparkly!
	#do_flame_effect()
	sprite.modulate = Color.white
	if orb_type != -1:
		sprite_orb_shine.show()
		orb_lit = true

func reset_burned() -> void:
	sprite.modulate = Color.gray
	sprite_orb_shine.hide()
	burned = false
	orb_lit = false

func emit_flame(emitter : Position2D) -> void:
	var flame : Sprite = _Flame.instance()
	add_child(flame)
	flame.global_position = emitter.global_position
	flame.velocity = Vector2(randf() - 0.5, randf() - 0.5) * 32.0
	flame.angular_velocity = (randf() - 0.5) * 4.0

func do_flame_effect() -> void:
	var emitters : Array = flame_emitters[pipe_type]
	for emitter in emitters:
		emit_flame(emitter)

func spin_anticlockwise() -> void:
	sprite_target_rotation -= PI/2
	rotation -= 1
	rotation = wrapi(rotation, 0, 4) # janky hack m8
	audio_spin.play()
	grid.refresh_connections()

func spin_clockwise() -> void:
	sprite_target_rotation += PI/2
	rotation += 1
	rotation = wrapi(rotation, 0, 4) # janky hack m8
	audio_spin.play()
	grid.refresh_connections()

func is_mouse_within() -> bool:
	return get_global_rect().has_point(get_global_mouse_position())

func _input(event : InputEvent) -> void:
	if not active: return
	if event is InputEventMouseButton and event.pressed:
		if is_mouse_within():
			if event.button_index == BUTTON_LEFT:
				spin_clockwise()
			elif event.button_index == BUTTON_RIGHT:
				spin_anticlockwise()

func _process(delta : float) -> void:
	sprite.rotation = lerp(sprite.rotation, sprite_target_rotation, delta * 10)
	shine_animation += delta * 10.0
	sprite_orb_shine.frame = int(wrapf(shine_animation, 0.0, 7.0))

func _ready() -> void:
	rotation = randi() % 4
	sprite_target_rotation = rotation * (PI/2)
	sprite.rotation = sprite_target_rotation
	sprite.frame = pipe_type
	shine_animation = randf() * 60.0
