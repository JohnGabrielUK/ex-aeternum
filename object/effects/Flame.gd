extends Sprite

var velocity : Vector2 = Vector2.ZERO
var angular_velocity : float = 0.0
var life : float = 1.0

func _process(delta : float) -> void:
	position += velocity * delta
	rotation += angular_velocity * delta
	life -= delta
	if life <= 0.0:
		queue_free()
	else:
		modulate.a = life

func _ready() -> void:
	frame = randi() % 16
