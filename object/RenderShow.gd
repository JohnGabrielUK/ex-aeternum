extends TextureRect

onready var timer_frametick : Timer = $Timer_FrameTick
onready var tween : Tween = $Tween

const stills : Array = [
	"altar", "blue_far_closed", "blue_far_open",
	"blue_near_closed", "blue_near_open", "green_far_closed", "green_far_open",
	"green_near_closed", "green_near_open", "red_far_closed", "red_far_open",
	"red_near_closed", "red_near_open", "blue_room", "green_room", "red_room"
]

var frames_altar : Array
var frames_spin_doors_open : Array
var frames_spin_doors_closed : Array
var frames_still : Dictionary

enum ANIM_STATE {ALTAR, SPINNING, STILL, FADING}

onready var current_state = ANIM_STATE.STILL

onready var animation_index : int = 0
onready var animation_stop : int # Not for loops
onready var next_still : String # Not for loops
onready var doors_closed : bool = true
onready var doors_closed_next : bool = true
onready var doors_closed_change_timer : int = -1

func load_frames() -> void:
	frames_altar = []
	frames_spin_doors_open = []
	frames_spin_doors_closed = []
	# Load altar frames
	for i in range(0, 30):
		var filename : String = "res://renders/anims/altar/f%04d.jpg" % i
		frames_altar.append(load(filename))
	# Load spinning frames
	for i in range(0, 120):
		var filename_open : String = "res://renders/anims/spin/doors_open/f%04d.jpg" % i
		var filename_closed : String = "res://renders/anims/spin/doors_closed/f%04d.jpg" % i
		frames_spin_doors_open.append(load(filename_open))
		frames_spin_doors_closed.append(load(filename_closed))
	# Load still frames
	for still in stills:
		var filename : String = "res://renders/stills/%s.jpg" % still
		frames_still[still] = load(filename)

func start_spin_animation(start_frame : int, end_frame : int, end_still : String, target_doors_closed : bool) -> void:
	current_state = ANIM_STATE.SPINNING
	animation_index = start_frame
	animation_stop = end_frame
	next_still = end_still
	if doors_closed != target_doors_closed:
		doors_closed_next = target_doors_closed
		doors_closed_change_timer = 5

func start_fade_animation(still_name : String) -> void:
	current_state = ANIM_STATE.FADING
	tween.interpolate_property(self, "modulate", modulate, Color.black, 0.125)
	tween.start()
	yield(tween, "tween_all_completed")
	texture = frames_still[still_name]
	tween.interpolate_property(self, "modulate", Color.black, Color.white, 0.125)
	tween.start()
	yield(tween, "tween_all_completed")
	current_state = ANIM_STATE.STILL

func start_fade_animation_to_altar() -> void:
	current_state = ANIM_STATE.FADING
	tween.interpolate_property(self, "modulate", modulate, Color.black, 0.125)
	tween.start()
	yield(tween, "tween_all_completed")
	current_state = ANIM_STATE.ALTAR
	tween.interpolate_property(self, "modulate", Color.black, Color.white, 0.125)
	tween.start()

func _frame_tick() -> void:
	match current_state:
		ANIM_STATE.ALTAR:
			# Lookit the pretty lights
			animation_index += 1
			if animation_index >= 30:
				animation_index = 0
			texture = frames_altar[animation_index]
		ANIM_STATE.SPINNING:
			# Changing between doors closed/open
			if doors_closed != doors_closed_next:
				doors_closed_change_timer -= 1
				if doors_closed_change_timer == 0:
					doors_closed = doors_closed_next
			# The animation itself
			animation_index += 1
			if animation_index >= animation_stop:
				texture = frames_still[next_still]
				current_state = ANIM_STATE.STILL
			else:
				if doors_closed:
					texture = frames_spin_doors_closed[animation_index]
				else:
					texture = frames_spin_doors_open[animation_index]

func _ready() -> void:
	load_frames()
	timer_frametick.wait_time = 1.0 / 30.0
	timer_frametick.start()
