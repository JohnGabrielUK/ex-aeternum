extends Control

const _PipePuzzle : PackedScene = preload("res://object/PipePuzzle.tscn")

onready var render_show : TextureRect = $RenderShow
onready var arrow_left : TextureRect = $Texture_ArrowLeft
onready var arrow_right : TextureRect = $Texture_ArrowRight
onready var arrow_up : TextureRect = $Texture_ArrowUp
onready var arrow_down : TextureRect = $Texture_ArrowDown
onready var label_end : Label = $Label_End
onready var audio_door_change : AudioStreamPlayer = $Audio_Door_Change
onready var audio_ambience_pool : AudioStreamPlayer = $Audio_Ambience_Pool
onready var audio_ambience_machine : AudioStreamPlayer = $Audio_Ambience_Machine
onready var tween : Tween = $Tween

enum LOOKING_AT {ALTAR, RED_DOOR, GREEN_DOOR, BLUE_DOOR, ALTAR_NEAR, RED_DOOR_NEAR, GREEN_DOOR_NEAR, BLUE_DOOR_NEAR, BLUE_ROOM, GREEN_ROOM, RED_ROOM}

onready var looking_at : int = LOOKING_AT.ALTAR
onready var red_door_closed : bool = true
onready var blue_door_closed : bool = false
onready var green_door_closed : bool = true

onready var arrow_bounce : float = 0.0

var puzzle_red : Control
var puzzle_blue : Control
var puzzle_green : Control

func update_doors() -> void:
	var red_door_closed_new : bool = true
	var blue_door_closed_new : bool = true
	var green_door_closed_new : bool = true
	for puzzle in [puzzle_red, puzzle_blue, puzzle_green]:
		if puzzle.is_orb_lit(0):
			red_door_closed_new = false
		if puzzle.is_orb_lit(1):
			green_door_closed_new = false
		if puzzle.is_orb_lit(2):
			blue_door_closed_new = false
	# Change 'em
	if red_door_closed_new != red_door_closed:
		red_door_closed = red_door_closed_new
		audio_door_change.play()
	if blue_door_closed_new != blue_door_closed:
		blue_door_closed = blue_door_closed_new
		audio_door_change.play()
	if green_door_closed_new != green_door_closed:
		green_door_closed = green_door_closed_new
		audio_door_change.play()

func change_arrows_within_puzzle_room() -> void:
	match looking_at:
		LOOKING_AT.RED_ROOM:
			set_arrows_visible(false, !red_door_closed, false, false)
		LOOKING_AT.BLUE_ROOM:
			set_arrows_visible(false, !blue_door_closed, false, false)
		LOOKING_AT.GREEN_ROOM:
			set_arrows_visible(false, !green_door_closed, false, false)

func pipe_changed() -> void:
	#yield(get_tree().create_timer(0.05), "timeout") # janky hack m8
	update_doors()
	change_arrows_within_puzzle_room()

func _look_left():
	# If we're spinning or at the altar, do nothing
	if render_show.current_state in [render_show.ANIM_STATE.SPINNING, render_show.ANIM_STATE.ALTAR, render_show.ANIM_STATE.FADING]:
		return
	# What do we want to look at next?
	match looking_at:
		LOOKING_AT.BLUE_DOOR:
			render_show.start_spin_animation(105, 120, "altar", false)
			looking_at = LOOKING_AT.ALTAR
		LOOKING_AT.RED_DOOR:
			var next_frame : String = "blue_far_closed" if blue_door_closed else "blue_far_open"
			render_show.start_spin_animation(90, 105, next_frame, blue_door_closed)
			looking_at = LOOKING_AT.BLUE_DOOR
		LOOKING_AT.GREEN_DOOR:
			var next_frame : String = "red_far_closed" if red_door_closed else "red_far_open"
			render_show.start_spin_animation(75, 90, next_frame, red_door_closed)
			looking_at = LOOKING_AT.RED_DOOR
		LOOKING_AT.ALTAR:
			var next_frame : String = "green_far_closed" if green_door_closed else "green_far_open"
			render_show.start_spin_animation(60, 75, next_frame, green_door_closed)
			looking_at = LOOKING_AT.GREEN_DOOR

func _look_right():
	# If we're spinning or at the altar, do nothing
	if render_show.current_state in [render_show.ANIM_STATE.SPINNING, render_show.ANIM_STATE.FADING]:
		return
	# What do we want to look at next?
	match looking_at:
		LOOKING_AT.BLUE_DOOR:
			var next_frame : String = "red_far_closed" if red_door_closed else "red_far_open"
			render_show.start_spin_animation(15, 30, next_frame, red_door_closed)
			looking_at = LOOKING_AT.RED_DOOR
		LOOKING_AT.RED_DOOR:
			var next_frame : String = "green_far_closed" if green_door_closed else "green_far_open"
			render_show.start_spin_animation(30, 45, next_frame, green_door_closed)
			looking_at = LOOKING_AT.GREEN_DOOR
		LOOKING_AT.GREEN_DOOR:
			render_show.start_spin_animation(45, 60, "altar", false)
			looking_at = LOOKING_AT.ALTAR
		LOOKING_AT.ALTAR:
			var next_frame : String = "blue_far_closed" if blue_door_closed else "blue_far_open"
			render_show.start_spin_animation(0, 15, next_frame, blue_door_closed)
			looking_at = LOOKING_AT.BLUE_DOOR

func _go_forwards():
	# If we're spinning or at the altar, do nothing
	if render_show.current_state in [render_show.ANIM_STATE.SPINNING, render_show.ANIM_STATE.FADING]:
		return
	# What do we want to look at next?
	match looking_at:
		LOOKING_AT.BLUE_DOOR:
			var next_frame : String = "blue_near_closed" if blue_door_closed else "blue_near_open"
			render_show.start_fade_animation(next_frame)
			looking_at = LOOKING_AT.BLUE_DOOR_NEAR
			set_arrows_visible(!blue_door_closed, true, false, false)
			audio_ambience_pool.volume_db = -20.0
			audio_ambience_machine.volume_db = -15.0
		LOOKING_AT.RED_DOOR:
			var next_frame : String = "red_near_closed" if red_door_closed else "red_near_open"
			render_show.start_fade_animation(next_frame)
			looking_at = LOOKING_AT.RED_DOOR_NEAR
			set_arrows_visible(!red_door_closed, true, false, false)
			audio_ambience_pool.volume_db = -20.0
			audio_ambience_machine.volume_db = -15.0
		LOOKING_AT.GREEN_DOOR:
			var next_frame : String = "green_near_closed" if green_door_closed else "green_near_open"
			render_show.start_fade_animation(next_frame)
			looking_at = LOOKING_AT.GREEN_DOOR_NEAR
			set_arrows_visible(!green_door_closed, true, false, false)
			audio_ambience_pool.volume_db = -20.0
			audio_ambience_machine.volume_db = -15.0
		LOOKING_AT.ALTAR:
			render_show.start_fade_animation_to_altar()
			looking_at = LOOKING_AT.ALTAR_NEAR
			set_arrows_visible(false, true, false, false)
			audio_ambience_pool.volume_db = 0.0
			audio_ambience_machine.volume_db = -40.0
		LOOKING_AT.BLUE_DOOR_NEAR:
			if not blue_door_closed:
				render_show.start_fade_animation("blue_room")
				looking_at = LOOKING_AT.BLUE_ROOM
				set_arrows_visible(false, true, false, false)
				audio_ambience_pool.volume_db = -40.0
				audio_ambience_machine.volume_db = 0.0
				setup_puzzle()
		LOOKING_AT.GREEN_DOOR_NEAR:
			if not green_door_closed:
				render_show.start_fade_animation("green_room")
				looking_at = LOOKING_AT.GREEN_ROOM
				set_arrows_visible(false, true, false, false)
				audio_ambience_pool.volume_db = -40.0
				audio_ambience_machine.volume_db = 0.0
				setup_puzzle()
		LOOKING_AT.RED_DOOR_NEAR:
			if not red_door_closed:
				render_show.start_fade_animation("red_room")
				looking_at = LOOKING_AT.RED_ROOM
				set_arrows_visible(false, true, false, false)
				audio_ambience_pool.volume_db = -40.0
				audio_ambience_machine.volume_db = 0.0
				setup_puzzle()
				label_end.show()

func _go_backwards():
	# If we're spinning or at the altar, do nothing
	if render_show.current_state in [render_show.ANIM_STATE.SPINNING, render_show.ANIM_STATE.FADING]:
		return
	# What do we want to look at next?
	match looking_at:
		LOOKING_AT.BLUE_DOOR_NEAR:
			var next_frame : String = "blue_far_closed" if blue_door_closed else "blue_far_open"
			render_show.start_fade_animation(next_frame)
			looking_at = LOOKING_AT.BLUE_DOOR
			set_arrows_visible(true, false, true, true)
			audio_ambience_pool.volume_db = -20.0
			audio_ambience_machine.volume_db = -30.0
		LOOKING_AT.RED_DOOR_NEAR:
			var next_frame : String = "red_far_closed" if red_door_closed else "red_far_open"
			render_show.start_fade_animation(next_frame)
			looking_at = LOOKING_AT.RED_DOOR
			set_arrows_visible(true, false, true, true)
			audio_ambience_pool.volume_db = -20.0
			audio_ambience_machine.volume_db = -30.0
		LOOKING_AT.GREEN_DOOR_NEAR:
			var next_frame : String = "green_far_closed" if green_door_closed else "green_far_open"
			render_show.start_fade_animation(next_frame)
			looking_at = LOOKING_AT.GREEN_DOOR
			set_arrows_visible(true, false, true, true)
			audio_ambience_pool.volume_db = -20.0
			audio_ambience_machine.volume_db = -30.0
		LOOKING_AT.ALTAR_NEAR:
			render_show.start_fade_animation("altar")
			looking_at = LOOKING_AT.ALTAR
			set_arrows_visible(true, false, true, true)
			audio_ambience_pool.volume_db = -10.0
			audio_ambience_machine.volume_db = -30.0
		LOOKING_AT.BLUE_ROOM:
			if not blue_door_closed:
				render_show.start_fade_animation("blue_near_open")
				looking_at = LOOKING_AT.BLUE_DOOR_NEAR
				set_arrows_visible(true, true, false, false)
				audio_ambience_pool.volume_db = -20.0
				audio_ambience_machine.volume_db = -15.0
				dismiss_puzzles()
		LOOKING_AT.GREEN_ROOM:
			if not green_door_closed:
				render_show.start_fade_animation("green_near_open")
				looking_at = LOOKING_AT.GREEN_DOOR_NEAR
				set_arrows_visible(true, true, false, false)
				audio_ambience_pool.volume_db = -20.0
				audio_ambience_machine.volume_db = -15.0
				dismiss_puzzles()
		LOOKING_AT.RED_ROOM:
			if not red_door_closed:
				render_show.start_fade_animation("red_near_open")
				looking_at = LOOKING_AT.RED_DOOR_NEAR
				set_arrows_visible(true, true, false, false)
				audio_ambience_pool.volume_db = -20.0
				audio_ambience_machine.volume_db = -15.0
				dismiss_puzzles()
				label_end.hide()

func setup_puzzle() -> void:
	yield(get_tree().create_timer(0.125), "timeout") # janky hack
	match looking_at:
		LOOKING_AT.RED_ROOM:
			puzzle_red.set_active(true)
			puzzle_red.show()
		LOOKING_AT.BLUE_ROOM:
			puzzle_blue.set_active(true)
			puzzle_blue.show()
		LOOKING_AT.GREEN_ROOM:
			puzzle_green.set_active(true)
			puzzle_green.show()

func dismiss_puzzles() -> void:
	for puzzle in [puzzle_red, puzzle_blue, puzzle_green]:
		puzzle.set_active(false)
		puzzle.hide()

func set_arrows_visible(up : bool, down : bool, left : bool, right : bool) -> void:
	tween.interpolate_property(arrow_up, "modulate", arrow_up.modulate, Color.white if up else Color.transparent, 0.25)
	tween.interpolate_property(arrow_down, "modulate", arrow_down.modulate, Color.white if down else Color.transparent, 0.25)
	tween.interpolate_property(arrow_left, "modulate", arrow_left.modulate, Color.white if left else Color.transparent, 0.25)
	tween.interpolate_property(arrow_right, "modulate", arrow_right.modulate, Color.white if right else Color.transparent, 0.25)
	tween.start()

func _process(delta : float) -> void:
	arrow_bounce += delta * 2.0
	arrow_left.rect_position.x = 16 + abs(sin(arrow_bounce) * 16)
	arrow_right.rect_position.x = 1200 - abs(sin(arrow_bounce) * 16)
	arrow_up.rect_position.y = 16 + abs(sin(arrow_bounce) * 16)
	arrow_down.rect_position.y = 640 - abs(sin(arrow_bounce) * 16)

func _ready() -> void:
	set_arrows_visible(true, false, true, true)
	puzzle_red = _PipePuzzle.instance()
	puzzle_blue = _PipePuzzle.instance()
	puzzle_green = _PipePuzzle.instance()
	for puzzle in [puzzle_red, puzzle_blue, puzzle_green]:
		puzzle.game = self
		add_child(puzzle)
	# Set up puzzle data
	puzzle_blue.pipe_grid.fill_grid(
		[
			4, 4, 4, 0, -1,
			-1, 1, 0, 0, -1,
			-1, 0, 2, 0, -1,
			-1, -1, 4, 0, -1
		],
		[
			2, 3, 1, -1, -1,
			-1, -1, -1, -1, -1,
			-1, -1, -1, -1, -1,
			-1, -1, 2, -1, -1,
		],
		5
	)
	puzzle_blue.pipe_grid.get_pipe_at_pos(Vector2(0, 0)).set_pipe_rotation(0)
	puzzle_blue.pipe_grid.get_pipe_at_pos(Vector2(1, 0)).set_pipe_rotation(2)
	puzzle_green.pipe_grid.fill_grid(
		[
			0, 0, -1,
			1, 0, 4,
			0, 1, 0,
			4, 1, 0
		],
		[
			-1, -1, -1,
			-1, -1, 0,
			-1, -1, -1,
			3, -1, -1
		],
		3
	)
	puzzle_blue.pipe_grid.refresh_connections()
	puzzle_green.pipe_grid.refresh_connections()
	dismiss_puzzles()
	# Audio
	audio_ambience_pool.play()
	audio_ambience_machine.play()
	# Fade in
	tween.interpolate_property(self, "modulate", Color.black, Color.white, 1.0)
	tween.start()

func _enter_tree() -> void:
	modulate = Color.black
