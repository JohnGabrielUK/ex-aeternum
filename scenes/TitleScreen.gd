extends Control

onready var texture_logo : TextureRect = $Texture_Logo
onready var label_prompt : Label = $Label_Prompt
onready var label_byline : Label = $Label_ByLine
onready var audio_bgm : AudioStreamPlayer = $Audio_BGM
onready var tween : Tween = $Tween

onready var active : bool = true

func _input(event : InputEvent) -> void:
	if not active: return
	if event is InputEventMouseButton and event.pressed and event.button_index == BUTTON_LEFT:
		active = false
		tween.interpolate_property(self, "modulate", Color.white, Color.black, 1.0)
		tween.interpolate_property(audio_bgm, "volume_db", audio_bgm.volume_db, -40, 1.0)
		tween.start()
		yield(tween, "tween_all_completed")
		get_tree().change_scene("res://scenes/Game.tscn")

func _ready() -> void:
	audio_bgm.play()
	yield(get_tree().create_timer(1.0), "timeout")
	for next in [texture_logo, label_prompt, label_byline]:
		tween.interpolate_property(next, "modulate", Color.transparent, Color.white, 2.0)
	tween.start()

func _enter_tree() -> void:
	for next in [$Texture_Logo, $Label_Prompt, $Label_ByLine]:
		next.modulate = Color.transparent
