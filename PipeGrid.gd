extends GridContainer

const _Pipe : PackedScene = preload("res://object/Pipe.tscn")
const _PipeEmpty : PackedScene = preload("res://object/PipeEmpty.tscn")

func reset_pipes() -> void:
	for child in get_children():
		if child is Pipe:
			child.reset_burned()

func get_pipe_at_pos(pos : Vector2) -> Node:
	for child in get_children():
		if child is Pipe:
			if child.board_position == pos:
				return child
	return null

func try_to_burn_tile(target : Vector2, from_direction : Vector2) -> void:
	var pipe_target : Node = get_pipe_at_pos(target)
	if pipe_target != null:
		if not pipe_target.burned:
			pipe_target.get_burned(from_direction)

func fill_grid(pipe_data : Array, grid_width : int) -> void:
	columns = grid_width
	for i in range(0, len(pipe_data)):
		var x : int = i % grid_width
		var y : int = i / grid_width
		var pipe_type : int = pipe_data[i]
		if pipe_type != -1:
			var pipe : Control = _Pipe.instance()
			pipe.pipe_type = pipe_type
			pipe.board_position = Vector2(x, y)
			add_child(pipe)
			pipe.connect("spread_fire", self, "try_to_burn_tile")
		else:
			add_child(_PipeEmpty.instance())

func _input(event : InputEvent) -> void:
	if event.is_action_pressed("ui_accept"):
		reset_pipes()
		try_to_burn_tile(Vector2(1, 0), Vector2.DOWN)

func _ready() -> void:
	fill_grid([
			-1, 4, 4, 0,
			4, 2, 0, 0,
			-1, 0, 2, 0,
			-1, -1, 4, 0
	], 4)
